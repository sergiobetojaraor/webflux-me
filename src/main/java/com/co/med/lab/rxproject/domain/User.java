package com.co.med.lab.rxproject.domain;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class User {

    private UUID identificacion;
    private String name;
    private Long telefono;
    private String estadoCivil;

}
