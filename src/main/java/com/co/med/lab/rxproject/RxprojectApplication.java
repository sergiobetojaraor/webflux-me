package com.co.med.lab.rxproject;

import com.co.med.lab.rxproject.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class RxprojectApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(RxprojectApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(RxprojectApplication.class, args);
		logger.debug("Running " + RxprojectApplication.class);
		List<User> users = obtainUserArrayList();
		users.stream().filter(user -> user.getTelefono() > 8).map(user -> user.getName()).forEach(System.out::println);
	}

	@Override
	public void run(String... args) throws Exception {
		Flux<String> nombres = Flux.just("Sergio", "Alberto", "Enrique", "Daniela", "Santiago")
				.doOnNext(e -> {
				if(e.isEmpty()){
					throw new RuntimeException("Error accediendo al dato");
					}
				})
				.filter(nom -> nom.startsWith("S"));

		List<User> usuarioFlux = new ArrayList<>();
		usuarioFlux.add(new User(UUID.randomUUID(), "Ser", 10L, "Soltero"));
		usuarioFlux.add(new User(UUID.randomUUID(), "Jar", 10L, "Soltero"));

		Flux.fromIterable(usuarioFlux)
				.flatMap(usuario -> {
					if(usuario.getName().equalsIgnoreCase("Ser")){
						return Mono.just(usuario);
					} else {
						return Mono.empty();
					}
				}).subscribe(e-> System.out.println(e.getName().concat(" Flux")));

		nombres.subscribe(e -> logger.info(e),
				e -> logger.error(e.getMessage()),
				new Runnable() {
					@Override
					public void run() {
						logger.info("Work done!");
					}
				});



	}
	private static List<User> obtainUserArrayList() {
		List<User> builderOfUsers = new ArrayList<>();
		builderOfUsers.add(new User(UUID.randomUUID(), "Sergio Orozco", 5L, "Unión Libre"));
		builderOfUsers.add(new User(UUID.randomUUID(), "Sergio Gómez", 700L, "Unión Libre"));
		builderOfUsers.add(new User(UUID.randomUUID(), "Sergio Osorio", 8L, "Unión Libre"));
		builderOfUsers.add(new User(UUID.randomUUID(), "Sergio Jaramillo", 928500L, "Unión Libre"));
		builderOfUsers.add(new User(UUID.randomUUID(), "Santiago Jaramillo", 9250L, "Unión Libre"));
		return builderOfUsers;
	}
}
