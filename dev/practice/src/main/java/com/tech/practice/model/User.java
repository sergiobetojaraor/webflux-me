package com.tech.practice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@Document(value = "User")
public class User implements Serializable {

    @Id
    private String id;
    private String name;
    private int favoriteNumber;
    private String company;

    public User(String id, String name, int favoriteNumber, String company){
        this.id = id;
        this.name = name;
        this.favoriteNumber = favoriteNumber;
        this.company = company;
    }
}
