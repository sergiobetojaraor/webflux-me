package com.tech.practice.controller;

import com.tech.practice.repository.UserRepository;
import com.tech.practice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/status")
    public String getStatus(){
        return "I'm alive";
    }

    @GetMapping("/users")
    public Flux<User> getAllUsers(){
        return userRepository.findAll();
    }

    @GetMapping("/user/{id}")
    public Mono<User> getUser(@PathVariable String id){
        return userRepository.findById(id);
    }

    @PostMapping("/user")
    public Mono<User> saveUser(@RequestBody User user){
        return userRepository.save(user);
    }

    @PutMapping("/user/{id}")
    public Mono<User> updateUser(@PathVariable String id, @RequestBody User user){
        return userRepository.save(user);
    }
}
